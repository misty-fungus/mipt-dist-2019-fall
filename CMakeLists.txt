cmake_minimum_required(VERSION 3.8)
project(tasks)

set(COURSE_ROOT ${CMAKE_CURRENT_LIST_DIR})

include(cmake/common.cmake)
include(cmake/zookeeper.cmake)
include(cmake/argagg.cmake)
include(cmake/Sanitize.cmake)

if (USE_PRIVATE)
    set(SOLUTIONS_ROOT ${CMAKE_SOURCE_DIR}/solutions)
else()
    set(SOLUTIONS_ROOT ${COURSE_ROOT}/solutions)
endif()

add_subdirectory(provision)
add_subdirectory(tasks/0-hello-world)
add_subdirectory(tasks/1-sequential-consumer)
add_subdirectory(tasks/2-parallel-consumer)
