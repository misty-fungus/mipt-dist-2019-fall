# Check configuration errors
apt-get install --fix-broken ${SIMULATE_APT_CHECK:+"--simulate"}

# Cleanup /var/log
find /var/log -iname '*.gz' -delete
find /var/log -iname '*.xz' -delete
find /var/log -iname '*.1' -delete
find /var/log -type f -print0 | xargs -0 -t tee >/dev/null 2>/dev/null

# Cleanup apt
apt-get clean
find /var/cache/apt/archives -iname '*.deb' -delete
find /var/lib/apt/lists -type f -delete
find /var/cache/apt -iname '*.bin' -delete

find tmp -mindepth 1 -delete

# Clean history
true > /root/.bash_history

# Debug
: + : + : + : + : + : + : + : + : + : + : + : + : + : + : + : + : + : + : + : +
dpkg -l || true
: + : + : + : + : + : + : + : + : + : + : + : + : + : + : + : + : + : + : + : +