#pragma once
#include <zookeeper.h>
#include <functional>
#include <cassert>
#include <future>
#include <compiler.hpp>
#include <defer.hpp>
#include <zk_consts.hpp>

namespace NSolution {
class TSolution {
 public:

  TSolution(std::string cluster, std::string root) :
      cluster_(std::move(cluster)), root_(std::move(root)) {
    // place your code here
  }

  void Run() {
    // place your code here
  }

 private:
  std::string cluster_;
  std::string root_;
  // and here
};
} // namespace NSolution