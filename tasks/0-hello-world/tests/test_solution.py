import logging
import os
import time

import kazoo.client
import kazoo.protocol.states as kazoo_states
from docker.models import networks as dm_networks

from dsys import log_setup
from dsys import docker_util
from dsys import netfilter
from dsys import zookeeper_cluster

log = logging.getLogger('test_hello_world')


def test_steady_network(docker_network, docker_client, zookeeper_dist, tmpdir, request):
    """

    :type docker_network: dm_networks.Network
    :type docker_client: docker.DockerClient
    :type zookeeper_dist: str
    :type tmpdir: py.path.local
    :type request:
    """
    log_setup.init_logging()
    user_solution = request.config.getoption('--solution')
    course_root = request.config.getoption('--course-root')
    assert os.path.exists(user_solution)
    build_root = request.config.getoption('--build-root')
    log_root = request.config.getoption('--logs-root')

    with zookeeper_cluster.ZookeeperCluster(docker_network, docker_client, zookeeper_dist, tmpdir) as zookeeper:
        time.sleep(3)  # Wait for cluster ....
        zookeeper_connection_string = zookeeper.connection_string
        cmd = [
            user_solution,
            '--cluster', zookeeper_connection_string,
            '--root', '/task0',
        ]
        log.warning('Will spawn CMD: {}'.format(cmd))

        zk_client = kazoo.client.KazooClient(zookeeper_connection_string)
        zk_client.start()
        zk_client.create('/task0')
        zk_client.create('/task0/hello_world', 'Hello ')

        user_container = docker_util.UserContainerWrapper(
            cmd,
            docker_client,
            docker_network,
            build_root,
            course_root,
            log_root,
        )
        user_container.create()
        try:
            user_container.start()
            rc = user_container.wait(10)
            assert rc == 0, 'User container crashed with code {}'.format(rc)
        finally:
            user_container.terminate()

        data, meta = zk_client.get('/task0/hello_world')  # type: (str, kazoo_states.ZnodeStat)
        assert (data == 'Hello World!') or (data == 'Hello world!')
        assert meta.version == 1

        try:
            zk_client.stop()
        except Exception:
            pass


def test_offline_start(docker_network, docker_client, zookeeper_dist, tmpdir, request):
    """

    :type docker_network: dm_networks.Network
    :type docker_client: docker.DockerClient
    :type zookeeper_dist: str
    :type tmpdir: py.path.local
    :type request:
    """
    log_setup.init_logging()
    user_solution = request.config.getoption('--solution')
    course_root = request.config.getoption('--course-root')
    assert os.path.exists(user_solution)
    build_root = request.config.getoption('--build-root')
    log_root = request.config.getoption('--logs-root')

    with netfilter.NetFilter(*docker_util.get_network_subnet(docker_network)) as net_filter:
        with zookeeper_cluster.ZookeeperCluster(docker_network, docker_client, zookeeper_dist, tmpdir) as zookeeper:
            zookeeper.register_at_netfilter(net_filter)
            zookeeper_connection_string = zookeeper.connection_string
            cmd = [
                user_solution,
                '--cluster', zookeeper_connection_string,
                '--root', '/task0',
            ]
            log.warning('Will spawn CMD: {}'.format(cmd))

            zk_client = kazoo.client.KazooClient(zookeeper_connection_string)
            zk_client.start()
            zk_client.create('/task0')
            zk_client.create('/task0/hello_world', 'Hello ')

            user_container = docker_util.UserContainerWrapper(
                cmd,
                docker_client,
                docker_network,
                build_root,
                course_root,
                log_root,
            )
            user_container.create()
            try:
                user_container.start()
                docker_util.print_network_containers(docker_network)
                time.sleep(10)

                user_container.container.reload()
                if user_container.ready():
                    msg = 'Container {} terminated abnormally: {}'.format(
                        user_container.container_name,
                        user_container.container.status,
                    )
                    log.error(msg)
                    user_container.wait(5)
                    raise AssertionError(msg)

                net_filter.connect_host(user_container.address)

                exit_code = user_container.wait(timeout=30)
                assert exit_code == 0
            finally:
                user_container.terminate()

            data, meta = zk_client.get('/task0/hello_world')  # type: (str, kazoo_states.ZnodeStat)
            assert data == 'Hello World!'
            assert meta.version == 1

            try:
                zk_client.stop()
            except Exception:
                pass
