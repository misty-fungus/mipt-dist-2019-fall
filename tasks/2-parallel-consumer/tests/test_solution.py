import datetime
import logging
import os
import random
import time
import threading

import kazoo.client
from docker.models import networks as dm_networks

from dsys import docker_util
from dsys import netfilter
from dsys import zookeeper_cluster
from dsys import log_setup
from dsys import interfaces
from dsys import steady_tester
from dsys import flaky_tester

log = logging.getLogger('test_parallel_consumer')


class DataProducer(interfaces.IProducer):

    NAME_TEMPLATE = 'task-{:010}'
    MODULO = 1581949

    def __init__(self, zk, root, tasks_max, tasks_batch, tasks_limit):
        """

        :type zk: kazoo.client.KazooClient
        :type root: str
        :type tasks_max: int
        :type tasks_batch: int
        :type tasks_limit: int
        """
        self.zk = zk
        self.root = root
        self.idx = 1
        self.aggr = 0
        self.rng = random.Random(12583)
        self.tasks_processed = 0
        self.tasks_produced = 0
        self.completed_ev_ = threading.Event()
        self.termination_ev_ = threading.Event()
        self.last_progress = datetime.datetime.min

        self.tasks_max = tasks_max
        self.tasks_batch = tasks_batch
        self.tasks_limit = tasks_limit

    @property
    def completed_ev(self):
        return self.completed_ev_

    @property
    def termination(self):
        return self.termination_ev_

    def add_terminator_task(self):
        name = self.NAME_TEMPLATE.format(self.idx)
        self.idx += 1
        log.warning('Producing termination task. Id: {}, Payload: "=", Result: {}'.format(
            name,
            self.aggr,
        ))

        self.zk.create(os.path.join(self.root, name), b'=')
        self.tasks_produced += 1
        return name

    def add_new_tasks(self):
        task_count = self.rng.randint(10, self.tasks_batch)
        for task in range(task_count):
            name = self.NAME_TEMPLATE.format(self.idx)
            self.idx += 1

            if self.rng.random() < 0.7 or self.aggr < 200:
                op = b'+'
                value = self.rng.randint(1, 500)
                self.aggr = (self.aggr + value) % self.MODULO
            else:
                op = b'-'
                value = self.rng.randint(1, 50)
                self.aggr = self.aggr - value

            payload = op + b' ' + str(value)

            log.warning('Producing task. Id: {}, Payload: "{}", Result: {}'.format(
                name,
                payload,
                self.aggr,
            ))
            self.zk.create(os.path.join(self.root, name), payload)
            self.tasks_produced += 1

    def __call__(self):
        it = 0
        self.last_progress = datetime.datetime.now()
        while self.termination_ev_.is_set() is False:
            it += 1
            log.warning('Producer iteration. Iteration: {}, Produced: {}, Consumed: {}, LastResultLag: {}'.format(
                it,
                self.tasks_produced,
                self.tasks_processed,
                datetime.datetime.now() - self.last_progress
            ))
            children = self.zk.get_children(self.root)
            delta_tasks = self.tasks_produced - self.tasks_processed - len(children)
            self.tasks_processed += delta_tasks

            if delta_tasks:
                self.last_progress = datetime.datetime.now()

            if datetime.datetime.now() - self.last_progress > datetime.timedelta(minutes=2):
                log.error('No progress in 2 minutes! Terminate.')
                self.termination_ev_.set()
                continue

            if self.tasks_produced <= self.tasks_max:
                if len(children) < self.tasks_limit:
                    self.add_new_tasks()

            if self.tasks_produced > self.tasks_max and len(children) == 0:
                self.completed_ev_.set()
                time.sleep(20)  # sleep 20s to let container taints be removed
                self.add_terminator_task()
                break

            time.sleep(0.1)


def test_steady_network(docker_network, docker_client, zookeeper_dist, tmpdir, request):
    """

    :type docker_network: dm_networks.Network
    :type docker_client: docker.DockerClient
    :type zookeeper_dist: str
    :type tmpdir: py.path.local
    :type request:
    """
    log_setup.init_logging()
    user_solution = request.config.getoption('--solution')
    course_root = request.config.getoption('--course-root')
    assert os.path.exists(user_solution)
    build_root = request.config.getoption('--build-root')
    log_root = request.config.getoption('--logs-root')

    queue_root = '/task2/queue'
    result_path = '/task2/result'

    with zookeeper_cluster.ZookeeperCluster(docker_network, docker_client, zookeeper_dist, tmpdir) as zookeeper:
        time.sleep(3)  # Wait for cluster ....
        zookeeper_connection_string = zookeeper.connection_string
        cmd = [
            user_solution,
            '--cluster', zookeeper_connection_string,
            '--root', os.path.dirname(queue_root),
        ]
        log.warning('Will spawn CMD: {}'.format(cmd))

        zk_client = kazoo.client.KazooClient(zookeeper_connection_string)
        zk_client.start()

        zk_client.create(queue_root, makepath=True)
        zk_client.create(result_path, b'0', makepath=True)

        producer = DataProducer(zk_client, queue_root, 3000, 200, 500)

        steady_tester.run_steady(cmd, docker_client, docker_network, build_root, course_root, producer, log_root)

        # check result
        result_data, result_stat = zk_client.get(result_path)
        assert int(result_data) == producer.aggr


def test_flaky_setup(docker_network, docker_client, zookeeper_dist, tmpdir, request):
    """

    :type docker_network: dm_networks.Network
    :type docker_client: docker.DockerClient
    :type zookeeper_dist: str
    :type tmpdir: py.path.local
    :type request:
    """
    log_setup.init_logging()
    user_solution = request.config.getoption('--solution')
    course_root = request.config.getoption('--course-root')
    assert os.path.exists(user_solution)
    build_root = request.config.getoption('--build-root')
    log_root = request.config.getoption('--logs-root')

    queue_root = '/task2/queue'
    result_path = '/task2/result'

    with netfilter.NetFilter(*docker_util.get_network_subnet(docker_network)) as net_filter:
        with zookeeper_cluster.ZookeeperCluster(docker_network, docker_client, zookeeper_dist, tmpdir) as zookeeper:
            time.sleep(3)  # Wait for cluster ....
            zookeeper_connection_string = zookeeper.connection_string
            cmd = [
                user_solution,
                '--cluster', zookeeper_connection_string,
                '--root', os.path.dirname(queue_root),
            ]
            log.warning('Will spawn CMD: {}'.format(cmd))
            taint_manager = docker_util.TaintManager(net_filter)
            zookeeper.register_at_netfilter(net_filter)

            zk_client = kazoo.client.KazooClient(zookeeper_connection_string)
            zk_client.start()

            zk_client.create(queue_root, makepath=True)
            zk_client.create(result_path, b'0', makepath=True)

            producer = DataProducer(zk_client, queue_root, 10000, 300, 1000)

            try:
                flaky_tester.run_flaky(
                    cmd,
                    docker_client,
                    docker_network,
                    build_root,
                    course_root,
                    producer,
                    taint_manager,
                    log_root,
                )
            except BaseException as err:
                log.exception('OBLOM {}'.format(err))
                try:
                    zk_client.close()
                except:
                    pass
                raise

            log.warning('Checking result ...')
            try:
                result_data, result_stat = zk_client.get(result_path)
                assert int(result_data) == producer.aggr
                log.warning('Result OK')
                zk_client.stop()
            except Exception as err:
                log.exception('Failed to check result: {}'.format(err))

