#pragma once
#include <zookeeper.h>
#include <functional>
#include <cassert>
#include <future>
#include <compiler.hpp>
#include <defer.hpp>
#include <zk_consts.hpp>
#include <aggregator.hpp>

namespace NSolution {

zhandle_t *ZooKeeperInit(const std::string &cluster, watcher_fn fn, const clientid_t *clientid, void *context) {
  return zookeeper_init(cluster.data(), fn, 3000, clientid, context, 0);
}


class TSolution {
 public:

  TSolution(std::string cluster, std::string root) :
      cluster_(std::move(cluster)), root_(std::move(root)) {
    // place your code here
  }

  void Run() {
    // place your code here
    auto zh = ZooKeeperInit(cluste_, nullptr, nullptr, nullptr);
    TAggregator aggregator {0};
    // ...
  }

 private:
  std::string cluster_;
  std::string root_;
  // and here
};
} // namespace NSolution