set(ZOO_VERSION 3.5.6)

set(ROOT ${COURSE_ROOT}/contrib/zookeeper-dist)
set(SRC ${ROOT}/zookeeper-client/zookeeper-client-c)
set(DST ${CMAKE_BINARY_DIR}/contrib/zookeeper)
set(PRE_GENERATED_SRC ${COURSE_ROOT}/contrib/zookeeper_generated_${ZOO_VERSION})

execute_process(
        COMMAND ${CMAKE_COMMAND} -E create_symlink ../../../zookeeper_generated_${ZOO_VERSION} ${SRC}/generated
        WORKING_DIRECTORY ${COURSE_ROOT}
)
set(THREADED ON)
add_definitions(-DTHREADED)
add_subdirectory(${SRC})
set_target_properties(zookeeper PROPERTIES
    INTERFACE_INCLUDE_DIRECTORIES     "${PRE_GENERATED_SRC}"
)

target_include_directories(
        zktest PRIVATE ${CMAKE_CURRENT_BINARY_DIR}/contrib/zookeeper-dist/zookeeper-client/zookeeper-client-c/include
)

