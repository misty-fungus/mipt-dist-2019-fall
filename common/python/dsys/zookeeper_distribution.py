import functools
import os
import pytest
import shutil
import tarfile

import requests

_ZOOKEEPER_VERSION = '3.5.5'


def pytest_addoption(parser):
    parser.addoption('--zookeeper', help='path to ZooKeeper directory')


@pytest.fixture(scope="session")
def zookeeper_dist(request):
    dist_path = request.config.option.zookeeper
    if dist_path is not None:
        return dist_path

    dist_path = ".zookeeper"
    if os.path.exists(dist_path):
        return os.path.abspath(dist_path)

    tmp_dist_path = dist_path + ".tmp"
    request.addfinalizer(functools.partial(shutil.rmtree, tmp_dist_path))

    if os.path.exists(tmp_dist_path):
        shutil.rmtree(tmp_dist_path)

    response, url = None, None
    urls = [
        "https://mirror.yandex.ru/mirrors/apache/zookeeper/",
        "http://apache.claz.org/zookeeper/",
        "http://archive.apache.org/dist/zookeeper/",
    ]
    for url_base in urls:  # Old versions are moved to archive
        url = url_base + "zookeeper-{0}/apache-zookeeper-{0}-bin.tar.gz".format(_ZOOKEEPER_VERSION)
        response = requests.get(url, stream=True)
        if response.status_code == requests.codes.ok:
            break
    else:
        raise Exception("{}: {}".format(url, response.reason))

    with tarfile.open(fileobj=response.raw, mode="r|gz") as dist:
        dist.extractall(tmp_dist_path)

    os.rename(os.path.join(tmp_dist_path, "apache-zookeeper-{}-bin".format(_ZOOKEEPER_VERSION)), dist_path)
    return os.path.abspath(dist_path)
