import docker
import os
import pytest
import logging
from docker.models import containers as dm_containers
from docker.models import networks as dm_networks

from . import docker_util

log = logging.getLogger('docker_fixtures')

TEST_NETWORK_NAME = 'dsys_bridge'


@pytest.fixture(scope="session")
def docker_client():
    """

    :rtype: docker.DockerClient
    """
    client = docker.from_env()
    yield client
    # for container in client.containers.list(all=True):
    #     log.warning('Removing container: {}'.format(container.short_id))
    #     container.remove(force=True)
    # for network in client.networks.list(names=[TEST_NETWORK_NAME]):
    #     log.info('Removing network: {}'.format(network.short_id))
    #     network.remove()


@pytest.fixture(scope="function")
def docker_network(docker_client):
    """

    :rtype: dm_networks.Network
    """

    net_name = TEST_NETWORK_NAME + '-e' + os.environ.get('EXECUTOR_NUMBER', '0')
    net = docker_client.networks.create(net_name, driver="bridge", internal=True)
    net.reload()
    device, subnet, gateway = docker_util.get_network_subnet(net)
    log.warning(
        'Created test network. (Name: {0.name}, Id: {0.short_id}, Device: {3}, Subnet: {1}, Gateway: {2})'.format(
            net, subnet, gateway, device
        )
    )
    yield net

    net.reload()
    for guest in net.containers:  # type: dm_containers.Container
        net.disconnect(guest.short_id)
    net.remove()


